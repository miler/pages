document.addEventListener('DOMContentLoaded', function()
{
	var data = {}
	var lang = ''
	const supportedLangs = ['en', 'es']
	const url = window.location.origin
	var searchParams = new URLSearchParams(window.location.search)

	const main = document.querySelector('main')
	const transitionElem = document.getElementById('transition')
	const gallery = document.getElementById('gallery_items')

	let project_item = null

	const dateOptions =
	{
		year: 'numeric',
		month: 'long',
	};

	const getData = async () =>
	{
		const response = await fetch('data.json')
		return await response.json()
	}

	function doProjectPage(id)
	{
		var t_project = document.getElementById('project_template').content.cloneNode(true)
		const project = data.projects[id]
		let project_cntx = t_project.getElementById('project_cntx')
		let project_meta = t_project.getElementById('project_meta')
		let project_pics = t_project.getElementById('project_pics')

		project_item = t_project.getElementById('project')

		let project_title = t_project.getElementById('project_title')
		let project_cat_title = t_project.getElementById('project_cat_title')
		let project_date_title = t_project.getElementById('project_date_title')
		let project_cat = document.createElement('dd')
		let project_date = document.createElement('dd')

		project_item.className = project.cat

		project_title.textContent = project.name[lang]
		project_cat_title.textContent = data.texts[lang].project_cat
		project_date_title.textContent = data.texts[lang].project_date

		for (textitem in project.intro)
		{
			let textNode = document.createElement('p')
			textNode.id = 'project_leading'
			textNode.textContent = project.intro[textitem][lang]
			project_cntx.parentNode.insertBefore(textNode, project_cntx)
		}

		project_cat.textContent = data.categories[lang][project.cat]
		project_date.textContent = project.date

		project_cat_title.parentNode.insertBefore(project_cat, project_cat_title.nextSibling)
		project_date_title.parentNode.insertBefore(project_date, project_date_title.nextSibling)

		for (imgitem in project.pics)
		{
			let listImgNode = document.createElement('li')
			let figure = document.createElement('figure')
			let imgNode = document.createElement('img')
			imgNode.setAttribute('alt', project.name[lang] + ' - ' + imgitem)
			project.pics[imgitem][lang] && imgNode.setAttribute('title', project.pics[imgitem][lang])
			imgNode.setAttribute('src', 'img/' + project.cat + '/large/' + id + '-' + eval(parseInt(imgitem) + 1) + '.' + project.pics[imgitem].format)
			figure.appendChild(imgNode)
			listImgNode.appendChild(figure)

			if (project.pics[imgitem][lang])
			{
				let caption = document.createElement('figcaption')
				caption.textContent = project.pics[imgitem][lang]
				figure.appendChild(caption)
			}

			project_pics.appendChild(listImgNode)
		}

		if (Array.isArray(project.process))
		{
			let project_prc_title = document.createElement('dt')
			let project_prc_link = document.createElement('a')
			let project_prc = document.createElement('dd')
			let project_process = document.createElement('section')
			let project_process_title = document.createElement('h3')
			
			project_prc_link.href = '#project_process'
			project_prc_link.textContent = data.texts[lang].project_process
			project_prc.textContent = null
			project_prc_title.id = 'project_prc_title'
			project_prc_title.appendChild(project_prc_link)
			project_meta.appendChild(project_prc_title)
			project_meta.appendChild(project_prc)
			project_process.id = 'project_process'
			
			project_process_title.textContent = data.texts[lang].project_process
			project_process.appendChild(project_process_title)

			for (textitem in project.process)
			{
				let textNode = document.createElement('p')
				textNode.textContent = project.process[textitem][lang]
				project_process.appendChild(textNode)
			}

			project_item.appendChild(project_process)
		}

		if (Array.isArray(project.outcome))
		{
			let project_out_title = document.createElement('dt')
			let project_out_link = document.createElement('a')
			let project_out = document.createElement('dd')
			let project_outcome = document.createElement('section')
			let project_outcome_title = document.createElement('h3')
			
			project_out_link.href = '#project_outcome'
			project_out_link.textContent = data.texts[lang].project_outcome
			project_out.textContent = null
			project_out_title.id = 'project_out_title'
			project_out_title.appendChild(project_out_link)
			project_meta.appendChild(project_out_title)
			project_meta.appendChild(project_out)
			project_outcome.id = 'project_outcome'
			
			project_outcome_title.textContent = data.texts[lang].project_outcome
			project_outcome.appendChild(project_outcome_title)

			for (textitem in project.outcome)
			{
				let textNode = document.createElement('p')
				textNode.textContent = project.outcome[textitem][lang]
				project_outcome.appendChild(textNode)
			}

			project_item.appendChild(project_outcome)
		}

		if (Array.isArray(project.related) && project.related.some(e => data.projects[e].shown == true))
		{
			let project_rel_title = document.createElement('dt')
			let project_rel = document.createElement('dd')
			project_rel_title.textContent = data.texts[lang].project_related
			project_rel_title.id = 'project_rel_title'
			project_meta.appendChild(project_rel_title)

			for (relatedItem in project.related)
			{
				let relatedProject = data.projects[project.related[relatedItem]]
				let link = document.createElement('a')
				link.textContent = relatedProject.name[lang]
				link.href = '#'
				link.title = relatedProject.name[lang]
				link.dataset.id = project.related[relatedItem]
				link.addEventListener('click', handleProjectClicks)
				project_rel.appendChild(link)
			}

			project_meta.appendChild(project_rel)
		}

		if (Array.isArray(project.refs))
		{
			let project_ref_title = document.createElement('dt')
			let project_ref_link = document.createElement('a')
			let project_ref = document.createElement('dd')
			let project_refs = document.createElement('section')
			let project_refs_title = document.createElement('h3')
			let project_refs_list = document.createElement('ul')
			
			project_ref_link.href = '#project_refs'
			project_ref_link.textContent = data.texts[lang].project_refs
			project_ref.textContent = null
			project_ref_title.id = 'project_ref_title'
			project_ref_title.appendChild(project_ref_link)
			project_meta.appendChild(project_ref_title)
			project_meta.appendChild(project_ref)
			project_refs.id = 'project_refs'
			
			project_refs_title.textContent = data.texts[lang].project_refs
			project_refs.appendChild(project_refs_title)

			for (textitem in project.refs)
			{
				let listNode = document.createElement('li')
				let link = document.createElement('a')
				link.textContent = project.refs[textitem].descr[lang]
				link.href = project.refs[textitem].url
				link.title = project.refs[textitem].descr[lang]
				link.setAttribute('target', '_blank')
				link.setAttribute('rel', 'noopener noreferrer')
				listNode.appendChild(link)
				project_refs_list.appendChild(listNode)
			}

			project_refs.appendChild(project_refs_list)
			project_item.appendChild(project_refs)
		}

		if (Array.isArray(project.notes))
		{
			let project_not_title = document.createElement('dt')
			let project_not_link = document.createElement('a')
			let project_not = document.createElement('dd')
			let project_notes = document.createElement('section')
			let project_notes_title = document.createElement('h3')
			let project_notes_list = document.createElement('ul')
			
			project_not_link.href = '#project_notes'
			project_not_link.textContent = data.texts[lang].project_notes
			project_not.textContent = null
			project_not_title.id = 'project_not_title'
			project_not_title.appendChild(project_not_link)
			project_meta.appendChild(project_not_title)
			project_meta.appendChild(project_not)
			project_notes.id = 'project_notes'
			
			project_notes_title.textContent = data.texts[lang].project_notes
			project_notes.appendChild(project_notes_title)

			for (textitem in project.notes)
			{
				let listNode = document.createElement('li')
				listNode.textContent = project.notes[textitem][lang]
				project_notes_list.appendChild(listNode)
			}

			project_notes.appendChild(project_notes_list)
			project_item.appendChild(project_notes)
		}

/*
		const related_projects = Object.entries(data.projects).filter(([key, value]) => value.cat === project.cat && value.shown === true)
		const related_projects_length = related_projects.length
		const project_index = related_projects.findIndex(([key, value]) => key === id)

		if (related_projects_length > 1)
		{
			const prev_project = related_projects[project_index - 1]
			const next_project = related_projects[project_index + 1]
			const project_nav = t_project.getElementById('project_navigation').content.cloneNode(true)

			if (next_project)
			{
				const next_id = next_project[0]
				const next_data = next_project[1]
				const next_template = project_nav.getElementById('next_project_template').content.cloneNode(true)
				const next_link = next_template.getElementById('next_project')
				const next_title = next_template.getElementById('next_project_title')
				const next_date = next_template.getElementById('next_project_date')
				const next_figure = next_template.getElementById('next_project_figure')
				const next_img = document.createElement('img')
				next_link.title = next_data.name[lang]
				next_link.dataset.id = next_id
				next_link.addEventListener('click', handleProjectClicks)
				next_title.textContent = next_data.name[lang]
				next_date.textContent = next_data.date
				next_img.src = 'img/' + next_data.cat + '/320-' + next_id + '.jpg'
				next_img.alt = next_data.name[lang]
				next_figure.appendChild(next_img)
				project_nav.firstElementChild.appendChild(next_link)
			}

			project_item.appendChild(project_nav)
		}
*/

		main.className = 'project'
		main.appendChild(t_project)
	}

	function doAbout(section = null)
	{
		let t_about = document.getElementById('about_template').content.cloneNode(true)
		let aboutWrapper = t_about.getElementById('about')
		let aboutMeta = aboutWrapper.querySelector('#abm_meta[data-lang="' + lang + '"]>dl')
		
		aboutWrapper.querySelectorAll('[data-lang]').forEach( element => element.setAttribute('hidden', '') )
		aboutWrapper.querySelectorAll('[data-lang="' + lang + '"]').forEach( element => element.removeAttribute('hidden') )

		function createMetaLink(id, text, target)
		{
			let meta_title = document.createElement('dt')
			let meta_description = document.createElement('dd')
			let meta_link = document.createElement('a')

			meta_title.id = 'abm_meta_' + id
			meta_link.href = '#abm_' + id
			meta_link.textContent = text

			meta_title.appendChild(meta_link)
			aboutMeta.appendChild(meta_title)
			aboutMeta.appendChild(meta_description)
		}
		
		function doAboutWorks()
		{
			let t_about_works = t_about.getElementById('about_works_template').content.cloneNode(true)
			let title = t_about_works.querySelector('h3')
			let container = t_about_works.querySelector('dl')
			title.textContent = data.texts[lang].about_works_title

			for (work in data.cv.works)
			{
				let t_workNode = t_about_works.getElementById('about_work_template').content.cloneNode(true)
				let workName = t_workNode.querySelector('dt')
				let startDate = data.cv.works[work].startyear && data.cv.works[work].startmonth ? new Date(Date.UTC(data.cv.works[work].startyear, data.cv.works[work].startmonth)) : null
				let endDate = data.cv.works[work].endyear && data.cv.works[work].endmonth ? new Date(Date.UTC(data.cv.works[work].endyear, data.cv.works[work].endmonth)) : null
				let dateNode = document.createElement('span')
				workName.textContent = data.cv.works[work].name[lang]
				!endDate && workName.setAttribute('current', '')
				t_workNode.querySelector('p').textContent = data.cv.works[work].description[lang]
				t_workNode.querySelector('span').textContent = data.cv.works[work].employeer[lang]
				dateNode.textContent = endDate ? startDate.toLocaleDateString(lang, dateOptions) + ' - ' + endDate.toLocaleDateString(lang, dateOptions) : startDate.toLocaleDateString(lang, dateOptions)
				workName.appendChild(dateNode)
				let workNodeClone = document.importNode(t_workNode, true)
				container.appendChild(workNodeClone)
			}

			let clone = document.importNode(t_about_works, true)
			aboutWrapper.appendChild(clone)
			createMetaLink('works', data.texts[lang].about_works_title, clone)
		}

		function doAboutFormation()
		{
			let t_about_formation = t_about.getElementById('about_formation_template').content.cloneNode(true)
			let title = t_about_formation.querySelector('h3')
			let container = t_about_formation.querySelector('dl')
			title.textContent = data.texts[lang].about_formation_title

			for (formation in data.cv.formation)
			{
				let t_formationNode = t_about_formation.getElementById('about_formation_template').content.cloneNode(true)
				let formationTitle = t_formationNode.querySelector('dt')
				let startDate = data.cv.formation[formation].startyear && data.cv.formation[formation].startmonth ? new Date(Date.UTC(data.cv.formation[formation].startyear, data.cv.formation[formation].startmonth)) : null
				let endDate = data.cv.formation[formation].endyear && data.cv.formation[formation].endmonth ? new Date(Date.UTC(data.cv.formation[formation].endyear, data.cv.formation[formation].endmonth)) : null
				let dateNode = document.createElement('span')
				formationTitle.textContent = data.cv.formation[formation].title[lang]
				data.cv.formation[formation].graded && formationTitle.setAttribute('graded', '')
				t_formationNode.querySelector('p').textContent = data.cv.formation[formation].institution
				dateNode.textContent = startDate.toLocaleString(lang, dateOptions) + ' - ' + endDate.toLocaleString(lang, dateOptions)

				if (data.cv.formation[formation].graded)
				{
					formationTitle.setAttribute('graded', '')
					t_formationNode.querySelector('span').textContent = data.texts[lang].about_graded
				}

				formationTitle.appendChild(dateNode)
				let formationNodeClone = document.importNode(t_formationNode, true)
				container.appendChild(formationNodeClone)
			}

			let clone = document.importNode(t_about_formation, true)
			aboutWrapper.appendChild(clone)
			createMetaLink('formation', data.texts[lang].about_formation_title, clone)
		}

		function doAboutSkills()
		{
			let t_about_skills = t_about.getElementById('about_skills_template').content.cloneNode(true)
			let title = t_about_skills.querySelector('h3')
			let container = t_about_skills.querySelector('dl')
			title.textContent = data.texts[lang].about_skills_title

			for (type in data.cv.skills)
			{
				let t_skillType = t_about_skills.getElementById('about_skill_template').content.cloneNode(true)
				let skillList = t_skillType.querySelector('ul')
				t_skillType.querySelector('dt').textContent = data.cv.skills[type].type[lang]

				for (skill in data.cv.skills[type].skills)
				{
					let skillNode = document.createElement('li')
					skillNode.textContent = data.cv.skills[type].skills[skill]
					skillList.appendChild(skillNode)
				}
				
				container.appendChild(t_skillType)
			}

			let clone = document.importNode(t_about_skills, true)
			aboutWrapper.appendChild(clone)
			createMetaLink('skills', data.texts[lang].about_skills_title, clone)
		}

		function doAboutLanguages()
		{
			let t_about_languages = t_about.getElementById('about_languages_template').content.cloneNode(true)
			let title = t_about_languages.querySelector('h3')
			let container = t_about_languages.querySelector('dl')
			title.textContent = data.texts[lang].about_languages_title

			for (language in data.cv.languages)
			{
				let t_languageNode = t_about_languages.getElementById('about_language_template').content.cloneNode(true)
				t_languageNode.querySelector('dt').textContent = data.cv.languages[language].language[lang]
				t_languageNode.querySelector('.barGraphValue').setAttribute('width', data.cv.languages[language].value)
				let languageNodeClone = document.importNode(t_languageNode, true)
				container.appendChild(languageNodeClone)
			}

			let clone = document.importNode(t_about_languages, true)
			aboutWrapper.appendChild(clone)
			createMetaLink('languages', data.texts[lang].about_languages_title, clone)
		}

		function doAboutInterests()
		{
			let t_about_interests = t_about.getElementById('about_interests_template').content.cloneNode(true)
			let title = t_about_interests.querySelector('h3')
			let container = t_about_interests.querySelector('dl')
			title.textContent = data.texts[lang].about_interests_title

			for (interest in data.cv.interests)
			{
				let t_interestNode = t_about_interests.getElementById('about_interest_template').content.cloneNode(true)
				t_interestNode.querySelector('dt').textContent = data.cv.interests[interest].interest[lang]
				t_interestNode.querySelector('dd').textContent = data.cv.interests[interest].description[lang]
				let interestNodeClone = document.importNode(t_interestNode, true)
				container.appendChild(interestNodeClone)
			}

			let clone = document.importNode(t_about_interests, true)
			aboutWrapper.appendChild(clone)
			createMetaLink('interests', data.texts[lang].about_interests_title, clone)
		}

		switch (section)
		{
			case 'works':
				doAboutWorks()
				break
			case 'formation':
				doAboutFormation()
				break
			case 'skills':
				doAboutSkills()
				break
			case 'languages':
				doAboutLanguages()
				break
			case 'interests':
				doAboutInterests()
				break
			case 'me':
				doAboutWorks()
				doAboutFormation()
				doAboutSkills()
				doAboutLanguages()
				doAboutInterests()
				break
			default:
				break
		}

		let clone = document.importNode(t_about, true)
		main.appendChild(clone)
	}

	function doGallery(filter = null)
	{
		let t_gallery_project = document.getElementById('gallery_project').content
		let t_gallery_filter = document.getElementById('gallery_filter').content

		if (data.projects && t_gallery_project && t_gallery_filter && gallery)
		{
			for (let c in data.categories[lang])
			{
				let input = t_gallery_filter.querySelector('input')
				let label = t_gallery_filter.querySelector('label')

				input.type = 'radio'
				input.id = 'gallery_filter_' + c

				label.setAttribute('for', 'gallery_filter_' + c)
				label.textContent = data.categories[lang][c]

				let clone = document.importNode(t_gallery_filter, true)
				gallery.parentNode.insertBefore(clone, gallery)

				if (filter === c )
				{
					document.getElementById(input.id).setAttribute('checked', '')
					document.getElementById('gallery_filter_all').removeAttribute('checked')
				}

				document.getElementById(input.id).addEventListener('click', function(event)
				{
					if (event.target.checked)
					{
						searchParams.set('filter', c)
						window.history.pushState({}, '', url + '?' + searchParams.toString())
					}
				})
			}

			for (let x in data.projects)
			{
				if (data.projects[x].shown == false) continue

				const project_link = t_gallery_project.querySelector('a')
				const project_img = t_gallery_project.querySelector('img')
				const project_img_sources = t_gallery_project.querySelectorAll('source')
				const project_title = t_gallery_project.querySelector('h4')
				const project_descr = t_gallery_project.querySelector('span')

				project_link.className = data.projects[x].cat
				project_link.dataset.id = x
				project_img_sources[0].srcset = 'img/' + data.projects[x].cat + '/320-' + x + '.jpg'
				project_img_sources[1].srcset = 'img/' + data.projects[x].cat + '/480-' + x + '.jpg'
				project_img_sources[2].srcset = 'img/' + data.projects[x].cat + '/640-' + x + '.jpg'
				project_img.srcset = 'img/' + data.projects[x].cat + '/640-' + x + '.jpg'
				project_img.alt = data.projects[x].name[lang]
				project_title.textContent = data.projects[x].name[lang]
				project_descr.textContent = data.projects[x].descr[lang]

				let project_item = document.importNode(t_gallery_project, true)
				project_item.querySelector('a').addEventListener('click', handleProjectClicks)
				gallery.appendChild(project_item)
			}
		}
	}

	function doPageChange(wasClicked, changeLang = false)
	{
		function checkImgLoad()
		{
			Promise.all(Array.from(document.images).filter(img => !img.complete)
				.map(img => new Promise(resolve =>
				{
					img.onload = img.onerror = resolve
				}))
			).then(() =>
			{
				document.body.dataset.status = 'loaded'
			})
		}

		function doProjectPageReady(project)
		{
			project_item && project_item.remove()
			doProjectPage(project)
			checkImgLoad()
		}

		function doAboutReady(section)
		{
			main.className = 'about'
			changeLang && document.getElementById('about').remove()
			document.getElementById('about')=== null && doAbout(section)
			project_item && project_item.remove()
			checkImgLoad()
		}

		function doGalleryReady()
		{
			const filter = searchParams.get('filter')

			if (changeLang)
			{
				gallery.replaceChildren()
				gallery.parentNode.querySelectorAll('input,label').forEach( element => element.remove() )
				gallery_filter_all = document.createElement('input')
				gallery_filter_all.id = 'gallery_filter_all'
				gallery_filter_all.setAttribute('type', 'radio')
				gallery_filter_all.setAttribute('name', 'gallery_filter')
				gallery_filter_all.setAttribute('hidden', '')
				!filter && gallery_filter_all.setAttribute('checked', '')
				gallery.parentNode.insertBefore(gallery_filter_all, gallery)
			}

			if (!gallery || gallery.childNodes.length <= 1)
			{
				doGallery(filter)
			}

			if (project_item && !changeLang)
			{
				project_item.remove()
			}

			checkImgLoad()
			!changeLang && main.removeAttribute('class')
		}

		if (searchParams.has('about'))
		{
			const aboutPart = searchParams.get('about')

			if (wasClicked)
			{
				document.body.dataset.status = 'leaving'
				transitionElem.addEventListener('animationend', function(){ doAboutReady(aboutPart) }, { once: true })
			}

			else
			{
				doAboutReady(aboutPart)
			}

			return
		}

		else if (changeLang)
		{
			aboutItem = document.getElementById('about')
			aboutItem && aboutItem.remove()
		}

		if (searchParams.has('project') && data.projects.hasOwnProperty(searchParams.get('project')))
		{
			const project = searchParams.get('project')

			if (wasClicked)
			{
				document.body.dataset.status = 'leaving'
				transitionElem.addEventListener('animationend', function()
				{
					doProjectPageReady(project)
					changeLang && doGalleryReady()
				},
				{ once: true })
			}

			else
			{
				document.body.dataset.status = 'loading'
				doProjectPageReady(project)
			}

			return
		}

		else
		{
			let status = document.body.dataset.status

			if (status === 'loaded')
			{
				document.body.dataset.status = 'leaving'
				transitionElem.addEventListener('animationend', doGalleryReady, { once: true })
			}

			else if (status === 'loading')
			{
				doGalleryReady()
			}
		}
	}

	function setLanguage(newLang = null)
	{
		function doChangeLang()
		{
			document.documentElement.lang = lang
			document.title = data.texts[lang].title
			document.querySelector('meta[name="description"]').setAttribute('content', data.texts[lang].about_copy)
			document.getElementById('logo_title').textContent = data.texts[lang].title

			document.querySelectorAll('[data-lang]').forEach(function(langItem)
			{
				if (langItem.getAttribute('data-lang') !== lang)
				{
					langItem.setAttribute('hidden', '')
				}

				else
				{
					langItem.removeAttribute('hidden')
				}
			})
		}

		if (newLang && newLang !== lang)
		{
			switch(newLang)
			{
				case 'es':
					lang = 'es'
					break
				case 'en':
					lang = 'en'
					break;
				default:
					lang = document.documentElement.lang
					searchParams.set('lang', lang)
					break
			}

			doPageChange(true, true)

			transitionElem.addEventListener('animationend', function()
			{
				document.body.dataset.status = 'loaded'
				doChangeLang()
			})
		}

		else
		{
			let userLang = navigator.language || navigator.userLanguage
			userLang = (userLang !== '') ? userLang.slice(0,2) : lang
			lang = (supportedLangs.includes(userLang)) ? userLang : document.documentElement.lang
			doChangeLang()
		}
	}

	function handleProjectClicks(event)
	{
		event.stopPropagation()
		event.preventDefault()
		searchParams.has('about') && searchParams.delete('about')
		event.currentTarget.dataset.id ? searchParams.set('project', event.currentTarget.dataset.id) : searchParams.delete('project')
		window.history.pushState({}, '', url + '?' + searchParams.toString())
		doPageChange(true)
	}

	window.addEventListener('popstate', function(event)
	{
		history.navigationMode = 'compatible'

		if (!document.location.hash)
		{
			searchParams = new URLSearchParams(window.location.search)
			doPageChange(true)
		}
	})

/*	document.addEventListener('keyup', function(event)
	{
		switch (event.code)
		{
			case "ArrowLeft":
				break;
			case "ArrowRight":
				break;
			default:
				break;
		}
	}*/

	getData()
	.then (json =>
	{
		data = json
		const lang_switchers = document.querySelectorAll('[data-langswitch]')
		const aboutmes = document.querySelectorAll('[data-type="abm"]')

		lang_switchers.forEach(function(lang_switcher)
		{
			lang_switcher.addEventListener('click', function(e)
			{
				e.stopPropagation()
				e.preventDefault()
				setLanguage(lang_switcher.getAttribute('data-langswitch'))
			})
		})

		aboutmes.forEach(function(aboutme)
		{
			aboutme.addEventListener('click', function(e)
			{
				e.stopPropagation()
				e.preventDefault()
				searchParams.set('about', 'me')
				searchParams.has('project') && searchParams.delete('project')
				window.history.pushState({}, '', url + '?' + searchParams.toString())
				doPageChange(true)
			})
		})

		document.querySelectorAll('.debug').forEach(function(debugItem)
		{
			debugItem.addEventListener('click', function(e)
			{
				e.stopPropagation()
				e.preventDefault()
				main.classList.toggle('grid')
			})
		})

		document.getElementById('header_logo').addEventListener('click', handleProjectClicks)
		setLanguage()
		doPageChange(false)
	})
	.catch(console.error())
})